﻿using DouchetteFonctions;
using System;

namespace Verification
{
    class Program
    {
        static void Main(string[] args)
        {
            BarCode test = new BarCode("per-2019-rz89");

            //variables where infos about product would be recorded
            string[] tab_for_type = new string[2];
            string[] tab_for_year = new string[2];
            string[] tab_for_ref = new string[2];

            //variables to verify the result of the verification's functions
            string v1 = "", v2 = "", v3 = "";

            //scan's result
            string scan_res = test.GetScan();

            //first check of scan's data
            test.GetTypeOfProduct(scan_res, tab_for_type);
            v1 = test.CheckingFirstPackage(tab_for_type);
            test.GetYearOfProduct(scan_res, tab_for_year, tab_for_type);
            v2 = test.CheckingDateOfProduct(tab_for_year);
            test.GetRefOfProduct(scan_res, tab_for_ref, tab_for_year);
            v3 = test.CheckingSecondAndThirdPackage(tab_for_year, tab_for_ref);

            //managing the potential errors of format or validity of date
            while (v1 != "true" || v2 != "true" || v3 != "true")
            {
                if (v1 != "true" || v2 != "true" || v3 != "true")
                {
                    //new entry of scan 
                    scan_res = test.NewEntryOfScan(scan_res);

                    //new check of scan's data
                    test.GetTypeOfProduct(scan_res, tab_for_type);
                    v1 = test.CheckingFirstPackage(tab_for_type);

                    test.GetYearOfProduct(scan_res, tab_for_year, tab_for_type);
                    v2 = test.CheckingDateOfProduct(tab_for_year);

                    test.GetRefOfProduct(scan_res, tab_for_ref, tab_for_year);
                    v3 = test.CheckingSecondAndThirdPackage(tab_for_year, tab_for_ref);
                }
            }

            //display of the wanted datas when they would be at the right format
            test.Display(tab_for_type, tab_for_year, tab_for_ref);

            //record of the data in the Database
            string save_path = "save.sqlite3";
            BDD Save = new BDD(save_path);

            //checking if path of database exists and execute the request to insert the datas of the scan
            if (Save.pathexists() == true)
            {
                //insert datas 
                bool res = Save.insert(tab_for_type[0], tab_for_year[0], tab_for_ref[0]);
                Console.WriteLine("Insert result => {0} ", res);
            }
            else
            {
                Console.WriteLine("No existing Database with the following path : {0}", Save.GetPath());
                Console.ReadKey();
            }
            //log out of database
            Save.Disconnect();
            Console.ReadKey();
        }
    }
}
