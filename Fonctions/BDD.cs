﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;

namespace DouchetteFonctions
{
    public class BDD
    {
        private SQLiteConnection Connect;
        private string path = "path.sqlite3";

        public BDD(string DB_path)
        {
            try
            {
                this.path = DB_path;
                bool exist_or_not = pathexists();

                Connect = new SQLiteConnection("Data Source=" + this.path + "; Version = 3;");
                Connect.Open();
                
                if(exist_or_not == false)

                    CreaTab(this.path);
            }
            catch (Exception e)
            {
                Console.WriteLine("New Exception has been thrown : constructorException => {0} " ,e.Message);
            }
        }

        public string GetPath()
        {
            return this.path;
        }
        public bool pathexists()
        {
            if (File.Exists(this.path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Disconnect()
        {
            Connect.Close();
        }
        public bool insert(string type, string year, string referency)
        {
            try
            {
                SQLiteCommand insert = new SQLiteCommand(Connect);
                insert.CommandText = ("insert into product (TYPE,YEAR,REF) Values (@type, @year, @referency);");
                insert.Parameters.AddWithValue("@type", type);
                insert.Parameters.AddWithValue("@year", year);
                insert.Parameters.AddWithValue("@referency", referency);
                insert.ExecuteNonQuery();

                return true;
            }
            catch (Exception e2)
            {
                Console.WriteLine("New exception has been thrown : insertException => {0} " ,e2.Message);
                return false;
            }
        }

        public bool CreaTab(string DB_path)
        {
            try
            {
                SQLiteCommand new_table = new SQLiteCommand(Connect);
                new_table.CommandText = "CREATE TABLE product (TYPE TEXT,YEAR TEXT,REF TEXT, [index] INTEGER PRIMARY KEY AUTOINCREMENT);";
                new_table.ExecuteNonQuery();

                return true;
            }
            catch (Exception e3)
            {
                Console.WriteLine("New Exception has been thrown : creaTabException => {0} " , e3.Message);
                return false;
            }
        }
    }
}
