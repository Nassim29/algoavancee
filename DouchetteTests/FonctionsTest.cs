﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DouchetteFonctions;

namespace DouchetteTests
{
    [TestClass]
    public class FonctionsTest
    {
        [TestMethod]
        public void TestGetTypeOfProduct()
        {
            BarCode test = new BarCode("PER-2019-RZ89");
            string[] type_test = new string[2];

            test.GetTypeOfProduct(test.GetScan(), type_test);
            Assert.AreEqual("PER", type_test[0]);
        }
        [TestMethod]
        public void TestGetYearOfProduct()
        {
            BarCode test = new BarCode("PER-2019-RZ89");

            string[] type_test = new string[2];
            string[] date_test = new string[2];

            test.GetTypeOfProduct(test.GetScan(), type_test);
            test.GetYearOfProduct(test.GetScan(), date_test, type_test);

            Assert.AreEqual("2019", date_test[0]);
        }

        [TestMethod]
        public void TestGetRefOfProduct()
        {
            BarCode test = new BarCode("PER-2019-RZ89");
            string[] type_test = new string[2];
            string[] date_test = new string[2];
            string[] ref_test = new string[2];

            test.GetTypeOfProduct(test.GetScan(), type_test);
            test.GetYearOfProduct(test.GetScan(), date_test, type_test);
            test.GetRefOfProduct(test.GetScan(), ref_test, date_test);

            Assert.AreEqual("RZ89", ref_test[0]);
        }

        [TestMethod]
        public void TestVerificationPremierPaquet()
        {
            BarCode test = new BarCode("PER-2019-RZ89");
            string[] type_test = new string[2];

            test.GetTypeOfProduct(test.GetScan() ,type_test);

            Assert.AreEqual(test.CheckingFirstPackage(type_test), "true");
        }

        [TestMethod]
        public void TestVerificationDateProduit()
        {
            BarCode test = new BarCode("PER-2019-RZ89");
            string[] type_test = new string[2];
            string[] date_test = new string[2];

            test.GetTypeOfProduct(test.GetScan(),type_test);
            test.GetYearOfProduct(test.GetScan(), date_test, type_test);

            Assert.AreEqual(test.CheckingDateOfProduct(date_test), "true");
        }

        [TestMethod]
        public void TestVerificationDeuxiemeEtTroisiemePaquet()
        {
            BarCode test = new BarCode("PER-2019-RZ89");

            string[] type_test = new string[2];
            string[] date_test = new string[2];
            string[] ref_test = new string[2];

            test.GetTypeOfProduct(test.GetScan(), type_test);
            test.GetYearOfProduct(test.GetScan(), date_test, type_test);
            test.GetRefOfProduct(test.GetScan(), ref_test, date_test);

            Assert.AreEqual(test.CheckingSecondAndThirdPackage(date_test, ref_test), "true");
        }
    }
}
