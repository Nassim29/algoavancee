﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DouchetteFonctions;

namespace DouchetteTests
{
    [TestClass]
    public class BDDTests
    {
        [TestMethod]
        public void pathexistsTest()
        {
            BDD test = new BDD("test.sqlite3");

            Assert.AreEqual(true, test.pathexists());
        }

        [TestMethod]
        public void insertTest()
        {
            BarCode code_test = new BarCode("per-2019-rz89");
            BDD test = new BDD("test.sqlite3");

            string[] type_test = new string[2];
            string[] date_test = new string[2];
            string[] ref_test = new string[2];

            code_test.GetTypeOfProduct(code_test.GetScan(), type_test);
            code_test.GetYearOfProduct(code_test.GetScan(), date_test, type_test);
            code_test.GetRefOfProduct(code_test.GetScan(), ref_test, date_test);

            Assert.AreEqual(true, test.insert(type_test[0],date_test[0], ref_test[0]));
        }

        [TestMethod]
        public void CreaTab()
        {
            BDD test = new BDD("test.sqlite3");

            string tmp = test.GetPath();

            Assert.AreEqual(false, test.CreaTab(tmp)); //we set the expected return value of CreaTab function on false because the table has been created at the creation of the database
        }
    }
}
